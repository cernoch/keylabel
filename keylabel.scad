module keylabel(
  text = "Label",
  width = 45,
  height = 14,
  thickness = 3,
  roundness = 2,
  label_margin = 3,
  hole_diameter = 6,
  hole_offset = 4)
{
  difference()
  {
    linear_extrude(thickness)
      difference()
      {
        translate(roundness*[1,1])
          minkowski()
          {
            circle(r=roundness, $fn=32);
            square([width,height] - roundness*[2,2]);
          }

        translate([hole_offset + hole_diameter/2, height/2])
          circle(d=hole_diameter, $fn=32);
      }

    translate([hole_offset+hole_diameter+label_margin,label_margin,thickness/2])
      linear_extrude(thickness)
        text(text,
          size = height - 2*label_margin,
          font = "Open Sans:style=Bold");
  }
}

keylabel();
