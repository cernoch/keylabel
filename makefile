STL=\
auto-rakev.stl\
bubenec.stl\
fabie-nosic.stl\
hybralec.stl\
keylabel.stl\
krasnoruzek.stl\
ovcin.stl\
prace.stl\
q-kolo.stl\
slatina.stl\
utechov.stl\

LIB=keylabel.scad

OPENSCAD=openscad
# Uncomment for Windows
# OPENSCAD="C:\Program Files\OpenSCAD\openscad.exe"



all: $(STL)

%.stl: %.scad $(LIB)
	$(OPENSCAD) -q -o $@ $<

clean:
	rm -f $(STL)
	